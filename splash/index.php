<!DOCTYPE html>
<html>
<head>
	<title>
		Grupo Tria
	</title>
	<link rel="stylesheet" type="text/css" href="style.css">
</head>
<body>
	<a href="mailto:info@grupotria.gt" style="position: absolute;top: 15%;left: 50%;transform: translate(-50%,0);">
		<img src="isotipo.png" alt="Isotipo Grupo Tria">
	</a>
	<img src="logo.png" alt="Logo Grupo Tria" style="position: absolute;bottom: 0;width: 25%;left: 50%;transform: translate(-50%,0);min-width: 320px;">


<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-85616793-1', 'auto');
  ga('send', 'pageview');

</script>
</body>
</html>
